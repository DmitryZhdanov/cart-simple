import api from '@/api'

import { filterByName, filterByCategory, defaultFilter } from '@/common'

const MUTATION_TYPE = {
	SET_PRODUCTS: 'SET_PRODUCTS',
	SET_CATEGORIES: 'SET_CATEGORIES',
	UPDATE_FILTER: 'UPDATE_FILTER',
	RESET_FILTERS: 'RESET_FILTERS'
}

export const state = () => ({
	products: [],
	categories: [],
	filter: { ...defaultFilter }
})

export const getters = ({
	filteredProducts: state => state.products
		.filter(filterByName(state.filter))
		.filter(filterByCategory(state.filter))
})

export const mutations = ({
	[MUTATION_TYPE.SET_PRODUCTS](state, payload) {
		state.products = payload
	},
	[MUTATION_TYPE.SET_CATEGORIES](state, payload) {
		state.categories = payload
	},
	[MUTATION_TYPE.UPDATE_FILTER](state, payload) {
		state.filter = {
			...state.filter,
			...payload
		}
	},
	[MUTATION_TYPE.RESET_FILTERS](state) {
		state.filter = { ...defaultFilter }
	}
})

export const actions = ({
	async fetchCategories({ commit }) {
		try {
			const { data } = await api.products.fetchCategories()
			commit(MUTATION_TYPE.SET_CATEGORIES, data)
		} catch {
			commit(MUTATION_TYPE.SET_CATEGORIES, [])
		}
	},
	async fetchProducts({ commit }) {
		try {
			const { data } = await api.products.fetchProducts()
			commit(MUTATION_TYPE.SET_PRODUCTS, data)
		} catch {
			commit(MUTATION_TYPE.SET_PRODUCTS, [])
		}
	}
})

export const MUTATION_TYPE = {
	ADD_TO_CART: 'ADD_TO_CART',
	REMOVE_FROM_CART: 'REMOVE_FROM_CART'
}

export const state = () => ({
	cart: []
})

export const getters = ({
	isItemInCart(state) {
		return id => !!~state.cart.findIndex(item => item.id === id)
	},
	cartCount(state) {
		return state.cart.length
	}
})

export const mutations = {
	[MUTATION_TYPE.ADD_TO_CART](state, payload) {
		state.cart.push(payload)
	},
	[MUTATION_TYPE.REMOVE_FROM_CART](state, id) {
		const index = state.cart.findIndex(product => product.id === id)

		if (~index) {
			state.cart.splice(index, 1)
		}
	}
}

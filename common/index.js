export const defaultFilter = {
	query: '',
	categories: [],
	typeOR: true
}

export const filterByName = ({ query }) => (item) => {
	return item.name.toLowerCase()
		.includes(query.toLowerCase())
}

export const filterByCategory = ({ categories, typeOR }) => (item) => {
	if (!categories.length) {
		return true
	}

	if (!item.category) {
		return false
	}

	const method = typeOR
		? 'some'
		: 'every'

	return categories[method]((catId) => {
		return ~item.category.findIndex(id => id === catId)
	})
}

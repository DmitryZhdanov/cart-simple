import axios from 'axios'
import Vue from 'vue'

class Api {
	instance = axios.create({
		baseURL: 'http://5dcb35ef34d54a0014314d38.mockapi.io/api/v1',
		responseType: 'json',
		headers: {
			'Content-Type': 'application/json'
		}
	})

	constructor () {
		// Register services
		const requireService = require.context('./service', false, /.js$/)

		requireService
			.keys()
			.forEach((filename) => {
				const name = /[^.](.+).js$/.exec(filename)[1]

				this[name] = requireService(filename).default(this.instance)
			})
	}

	intercept(fn) {
		fn(this.instance.interceptors)
	}

	install(Vue) {
		Vue.prototype.$api = this
	}
}

const api = new Api()

Vue.use(api)

export default api

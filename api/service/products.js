export default ({ get }) => ({
	fetchProducts() {
		return get('/products')
	},
	fetchCategories() {
		return get('/categories')
	}
})

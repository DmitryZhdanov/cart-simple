module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  // add your custom rules here
  rules: {
    'vue/html-indent': 'off',
    indent: ['error', 'tab'],
    'no-tabs': 'off',
    'space-before-function-paren': 'off',
    'no-console': 'off'
  }
}
